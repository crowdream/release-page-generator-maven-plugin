<!--
#-------------------------------------------------------------------------------
# Copyright (C) 2019 David Gammon
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#-------------------------------------------------------------------------------
-->

# About
This is a Maven plugin that is used to generate a release page based on commit messages.
The releases are derived from Git tags and the items in the release come from commit messages.

Both releases and release items must match configurable regular expressions. 

Each expression must define a number of groups.

By default (see below for how to change) the tags should be named **RELEASE-1.0.0** and commit messages should follow the format ***Bug: Fix for really hard bug**.

## An Example Page Generated Using the Defaults
![Example of Default Template (CSS not provided).](example.png)


# Example

## Minimal Example
```XML
<plugin>
	<groupId>io.cardiff.maven</groupId>
	<artifactId>release-page-generator-maven-plugin</artifactId>
	<version>1.0.0</version>
	<executions>
		<execution>
			<phase>generate-resources</phase>
			<goals>
				<goal>generate</goal>
			</goals>
		</execution>
	</executions>
</plugin>
```

## Complete Example
```XML
<plugin>
	<groupId>io.cardiff.maven</groupId>
	<artifactId>release-page-generator-maven-plugin</artifactId>
	<version>1.0.0</version>
	<executions>
		<execution>
			<phase>generate-resources</phase>
			<goals>
				<goal>generate</goal>
			</goals>
		</execution>
	</executions>
	
	<configuration>
		<output>target/release.html</output> <!-- Where to output the generated file. -->
		<tagRegex>^(.+)	(RELEASE-(.+))$</tagRegex> <!-- Regular expression to derive the release date (1) and release name (2) and version (3). Must contain 3 groups. -->
		<logRegex>\*([^:]+):(.*)$</logRegex> <!-- Regular expression to derive the commit type (1) and description (2). Must contain 2 groups. 
		<template>release.html</template> <!-- Classpath velocity resource template. 
		<properties> <!-- Properties that can be accessed in the template. See "release.html" for how to access.
			<property>stylesheet=css/release.css</property>
		</properties>
	</configuration>
</plugin>
```  
