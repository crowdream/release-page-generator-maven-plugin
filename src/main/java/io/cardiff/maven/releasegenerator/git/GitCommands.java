/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator.git;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import io.cardiff.maven.releasegenerator.model.Release;
import io.cardiff.maven.releasegenerator.model.ReleaseItem;

/**
 * Class that runs git commands to get release information.
 * @author David Gammon
 */
public class GitCommands {
    /** Logger for this class. */
    private static final Log LOG = LogFactory.getLog(GitCommands.class);
    /** Newline character. */
    private static final String NEW_LINE = "\n";
    /** Command that gets the logs for a specific tag. */
    private static final String CMD_GIT_LOG = "git log --format=%%B %s";
    /** Command that gets the logs for a specific tag and a previous tag. */
    private static final String CMD_GIT_LOG_WITH_PREV = "git log --format=%%B %s..%s";
    /** Command that get the git tags. */
    private static final String CMD_GIT_TAG = "git tag --format=%(taggerdate:iso)%09%(refname:strip=2)";
    /** Date format. */
    private static final String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss Z";
    /** Regex used to get the feature log. */
    private final Pattern logPattern;
    /** Regex used to get release tags. */
    private final Pattern tagPattern;
    /** Used to run commands. */
    private final CommandRunner commandRunner;
    
    /**
     * Create a new valid class.
     * @param tagPattern    The pattern to use to get release information.
     * @param logPattern    The pattern used to get release items.
     * @param commandRunner Used to run the Git commands.
     */
    public GitCommands(final String tagPattern, final String logPattern, final CommandRunner commandRunner) {
        this.tagPattern = Pattern.compile(tagPattern);
        this.logPattern = Pattern.compile(logPattern);
        this.commandRunner = commandRunner;
    }
    
    /**
     * Get releases sorted by version.
     * @return                          The release {@link Tag}s.
     * @throws CommandFailedException   Thrown on Git command errors.
     */
    public Release[] getReleases() throws CommandFailedException {
        final String releaseResult = commandRunner.runCommand(CMD_GIT_TAG);
        
        final List<Release> releases = new ArrayList<>();
        
        Release previousRelease = null;
        
        for (final String releaseStr : releaseResult.split(NEW_LINE)) {
            final Release release = toRelease(releaseStr);
            
            if (release != null) {
                addReleaseItems(release, previousRelease);
                
                releases.add(release);
            }
            
            previousRelease = release;
        }
        
        releases.sort(new ReleaseSorter());
        
        return releases.toArray(new Release[releases.size()]);
    }
    
    /**
     * Add the logs to the given {@link Release}. If the previousTag is null then only the logs for
     * the tag is used.
     * @param release                       The tag to get the logs for.
     * @param previousRelease               The previous tag to filter out changes that were not made in the tag.
     * @throws CommandFailedException   Thrown on failed commands.
     */
    private void addReleaseItems(final Release release, final Release previousRelease) throws CommandFailedException {
        String logs;
        
        if (previousRelease == null) {
            logs = commandRunner.runCommand(String.format(CMD_GIT_LOG, release.getName()));
        } else {
            logs = commandRunner.runCommand(String.format(CMD_GIT_LOG_WITH_PREV, previousRelease.getName(), release.getName()));
        }
        
        for (final String log : logs.split(NEW_LINE)) {
            final Matcher matcher = logPattern.matcher(log);
            
            LOG.debug("Processing log " + log);
            
            if (matcher.find()) {
                final String type = StringUtils.capitalize(matcher.group(1));
                final String description = matcher.group(2).trim();
                
                release.addItem(new ReleaseItem(type, description));
            } else {
                LOG.debug("Not matched log " + log);
            }
        }
    }

    /**
     * Convert a string to a date.
     * @param dateStr   The date string to turn into a date.
     * @return  The parsed {@link Date}.
     */
    private Date toDate(final String dateStr) {
        final SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
        
        try {
            return format.parse(dateStr);
        } catch (final ParseException e) {
            throw new IllegalStateException(String.format("Invalid date format got for date string: %s", dateStr));
        }
    }
    
    /**
     * Convert a tag name to a {@link Release}. Will not create a tag for non release tags.
     * @param name  The name of the tag.
     * @return      The {@link Release}. Could be null.
     */
    private Release toRelease(final String name) {
        final Matcher matcher = tagPattern.matcher(name);
        
        Release tag = null;
        
        // This is a tag we want.
        if (matcher.find()) {
            final String dateStr = matcher.group(1);
            final Date creationDate = toDate(dateStr);
            final String releaseName = matcher.group(2);
            final String version = matcher.group(3);
            
            tag = new Release(releaseName, version, creationDate);
        }
        
        return tag;
    }
    
    /**
     * Compares two releases for sorting.
     * @author David Gammon
     */
    private static class ReleaseSorter implements Comparator<Release> {

        /**
         * {@inheritDoc}
         */
        @Override
        public int compare(final Release o1, final Release o2) {
            return o2.getCreationDate().compareTo(o1.getCreationDate());
        }
    }
}
