/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator.git;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class used to run commands.
 * @author David Gammon
 */
public class CommandRunner {
    /** Logger for this class. */
    private static final Log LOG = LogFactory.getLog(CommandRunner.class);
    
    /**
     * Run the given command.
     * @param command                   The command to run.
     * @return                          The result of running the command.
     * @throws CommandFailedException   Thrown when the command fails.
     */
    public String runCommand(final String command) throws CommandFailedException {
        String result;
        
        LOG.debug("Running command " + command);
        
        try {
            final Process process = Runtime.getRuntime().exec(command);
            final InputStream in = process.getInputStream();
            result = IOUtils.toString(in, StandardCharsets.UTF_8);
            
        } catch (final IOException e) {
            throw new CommandFailedException(command, e);
        }
        
        LOG.debug("Command output is \"" + result + "\"");
        
        return result;
    }
}
