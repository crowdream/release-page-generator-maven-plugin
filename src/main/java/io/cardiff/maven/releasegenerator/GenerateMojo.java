/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import io.cardiff.maven.releasegenerator.git.CommandFailedException;
import io.cardiff.maven.releasegenerator.git.CommandRunner;
import io.cardiff.maven.releasegenerator.git.GitCommands;
import io.cardiff.maven.releasegenerator.model.Release;
import io.cardiff.maven.releasegenerator.template.TemplateEngine;
import io.cardiff.maven.releasegenerator.template.TemplateGenerationFailedException;

/**
 * Mojo used to generate a release page.
 * @author David Gammon
 */
@Mojo(name = "generate")
public class GenerateMojo extends AbstractMojo {
    /** The default output location if none is set. */
    public static final String OUTPUT = "target/release.html";
    /** The default log regular expression. Used to detected logs to inclusion. */
    public static final String LOG_REGEX = "\\*([^:]+):(.*)$";
    /** The default release detection regular expression. */
    public static final String TAG_REGEX = "^(.+)\t(RELEASE-(.+))$";
    /** Default classpath resource for the template. */
    public static final String TEMPLATE = "release.html";
    
    /** Instance of the writer. Used for testing. */
    private Writer writer;
    /** Commands used to get release information. */
    private GitCommands gitCommands;
    
    /** The location to write the output to. */
    @Parameter(property = "output", defaultValue = OUTPUT)
    private String output;
    /** The regular expression used to filter tags. */
    @Parameter(property = "tagRegex", defaultValue = TAG_REGEX)
    private String tagRegex;
    /** The regular expression used to filter log messages. */
    @Parameter(property = "logRegex", defaultValue = LOG_REGEX)
    private String logRegex;
    /** Where the template is. */
    @Parameter(property = "template", defaultValue = TEMPLATE)
    private String template;
    
    /** Additional properties to add to the template context. */
    @Parameter(property = "properties")
    private List<String> properties;
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            final Release[] release = getCommands().getReleases();
            
            try (final Writer writer = getWriter()) {
                TemplateEngine.generate(template, release, writer, loadProperties());
            }
        } catch (final CommandFailedException e) {
            throw new MojoFailureException("Could not run git commands", e);
        } catch (final TemplateGenerationFailedException e) {
            throw new MojoFailureException("Could generate file", e);
        } catch (final IOException e) {
            throw new MojoFailureException("Could not open file for writing", e);
        }
        
        getLog().info(String.format("Generated file %s", output));
    }

    /**
     * Load properties from the {@link #properties} list.
     * @return  The properties that were loaded.
     */
    private Map<String, String> loadProperties() {
        final Map<String, String> properties = new HashMap<>();
        
        if (this.properties != null) {
            for (final String property : this.properties) {
                final String[] parts = property.split("=");
                
                if (parts.length != 2) continue;
                
                properties.put(parts[0], parts[1]);
            }
        }
        
        return properties;
    }

    /**
     * Set the value of {@link #output}.
     * @param output The value to set {@link #output}.
     */
    public void setOutput(final String output) {
        this.output = output;
    }

    /**
     * Set the value of {@link #tagRegex}.
     * @param tagRegex The value to set {@link #tagRegex}.
     */
    public void setTagRegex(final String tagRegex) {
        this.tagRegex = tagRegex;
    }

    /**
     * Set the value of {@link #logRegex}.
     * @param logRegex The value to set {@link #logRegex}.
     */
    public void setLogRegex(final String logRegex) {
        this.logRegex = logRegex;
    }

    /**
     * Set the value of {@link #template}.
     * @param template The value to set {@link #template}.
     */
    public void setTemplate(final String template) {
        this.template = template;
    }
    
    /**
     * Set the value of {@link #properties}.
     * @param properties The value to set {@link #properties}.
     */
    public void setProperties(final List<String> properties) {
        this.properties = properties;
    }

    /**
     * Load a writer. Used for testing.
     * @param writer    The writer to load.
     */
    void loadWriter(final Writer writer) {
        this.writer = writer;
    }
    
    /**
     * Load a {@link GitCommands}. Used for testing.
     * @param commands    The commands to load.
     */
    void loadCommands(final GitCommands commands) {
        this.gitCommands = commands;
    }
    
    /**
     * Get the writer used for writing the result of generation. If there is no
     * instance of the writer then the {@link #output} location is used.
     * @return                  The {@link Writer}.
     * @throws IOException      Thrown if a new writer is created and it fails.
     */
    private Writer getWriter() throws IOException {
        final Writer writer;
        
        if (this.writer != null) {
            writer = this.writer;
        } else {
            final File parentFolder = new File(output).getParentFile();
            
            if (!parentFolder.exists()) parentFolder.mkdirs();
            
            writer = new FileWriter(output);
        }
        
        return writer;
    }
    
    /**
     * Get the commands to get release information. If there is no instance then
     * a new one is created. Used for testing.
     * @return  The commands.
     */
    private GitCommands getCommands() {
        final GitCommands commands;
        
        if (this.gitCommands != null) {
            commands = this.gitCommands;
        } else {
            commands = new GitCommands(tagRegex, logRegex, new CommandRunner());
        }
        
        return commands;
    }
}

