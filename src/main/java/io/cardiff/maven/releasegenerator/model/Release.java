/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author David Gammon
 *
 */
public class Release {
    /** The un-parsed name of this release. */
    private final String name;
    /** The version of this release. */
    private final String version;
    /** The date of creation for this release. */
    private final Date creationDate;
    /** The items for this release. */
    private final List<ReleaseItem> items = new ArrayList<>();
    
    /**
     * Create a new valid release.
     * @param name          The name of this release.
     * @param version       The version of this release.
     * @param creationDate  The date of creation.
     */
    public Release(final String name, final String version, final Date creationDate) {
        this.name = name;
        this.version = version;
        this.creationDate = creationDate;
    }

    /**
     * Simple getter for {@link #name}.
     * @return The value of {@link #name}. 
     */
    public String getName() {
        return name;
    }

    /**
     * Simple getter for {@link #version}.
     * @return The value of {@link #version}. 
     */
    public String getVersion() {
        return version;
    }

    /**
     * Simple getter for {@link #items}.
     * @return The value of {@link #items}. 
     */
    public ReleaseItem[] getItems() {
        return items.toArray(new ReleaseItem[items.size()]);
    }
    
    /**
     * Simple getter for {@link #creationDate}.
     * @return The value of {@link #creationDate}. 
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Add an item to this release.
     * @param item  The item.
     */
    public void addItem(final ReleaseItem item) {
        items.add(item);
    }
}
