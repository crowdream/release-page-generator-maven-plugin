/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator.model;

/**
 * An item that a release contains.
 * @author David Gammon
 */
public class ReleaseItem {
    /** The type of the release item. */
    private final String type;
    /** The description of the release item. */
    private final String description;
    
    /**
     * Create a new valid release item.
     * @param type             The type of the item.
     * @param description      The description for this item.
     */
    public ReleaseItem(final String type, final String description) {
        this.type = type;
        this.description = description;
    }

    /**
     * Simple getter for {@link #type}.
     * @return The value of {@link #type}. 
     */
    public String getType() {
        return type;
    }

    /**
     * Simple getter for {@link #description}.
     * @return The value of {@link #description}. 
     */
    public String getDescription() {
        return description;
    }
}
