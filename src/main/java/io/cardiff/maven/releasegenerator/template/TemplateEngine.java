/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator.template;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.tools.generic.DateTool;

import io.cardiff.maven.releasegenerator.model.Release;

/**
 * Class used to generate release information.
 * @author David Gammon
 */
public final class TemplateEngine {
    public static final String OUTPUT = "target" + File.pathSeparator + "release.html";
    /**
     * Hide the constructor.
     */
    private TemplateEngine() {
        // Nothing to see here.
    }
    
    /**
     * Generate the release page using the given tags and template.
     * @param templateLocation                      The location of the template.
     * @param releases                              The releases used to populate the template.
     * @param writer                                The writer to write the output to.
     * @param properties                            The properties to add to the context.
     * @throws TemplateGenerationFailedException    Thrown on generation errors.
     */
    public static void generate(final String templateLocation, final Release[] releases, final Writer writer, 
                                final Map<String, String> properties) throws TemplateGenerationFailedException {
        final Template template = getTemplate(templateLocation);
        
        final VelocityContext context = new VelocityContext();
        context.put("DateTool", new DateTool());
        context.put("releases", releases);
        context.put("properties", properties);
        
        final StringWriter out = new StringWriter();
        
        template.merge(context, out);
        
        final String result = out.toString();
        
        try {
            writer.write(result);
        } catch (final IOException e) {
            throw new TemplateGenerationFailedException(templateLocation, e);
        }
    }

    /**
     * Get the template for the given location.
     * @param templateLocation  The template location.
     * @return                  The loaded template.
     */
    private static Template getTemplate(final String templateLocation) {
        final VelocityEngine velocity = new VelocityEngine();
        velocity.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocity.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        
        final Template template = velocity.getTemplate(templateLocation, "UTF-8");
        return template;
    }
}
