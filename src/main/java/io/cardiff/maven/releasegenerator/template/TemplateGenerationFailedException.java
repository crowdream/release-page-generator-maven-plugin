/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator.template;

/**
 * Thrown when template generation fails.
 * @author David Gammon
 */
public class TemplateGenerationFailedException extends Exception {
    /** Serialisation UID. */
    private static final long serialVersionUID = 1L;
    /** Template message used to generate message for this exception. */
    private static final String MSG = "Failed to generate template from %s";
    
    /**
     * Create a new valid exception.
     * @param template  The template that caused this exception.
     * @param cause     The cause of this exception.
     */
    public TemplateGenerationFailedException(final String template, final Throwable cause) {
        super(String.format(MSG, template), cause);
    }
}
