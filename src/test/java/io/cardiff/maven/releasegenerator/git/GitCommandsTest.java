/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator.git;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import io.cardiff.maven.releasegenerator.GenerateMojo;
import io.cardiff.maven.releasegenerator.model.Release;
import io.cardiff.maven.releasegenerator.model.ReleaseItem;

/**
 * Unit tests for {@link GitCommands}.
 * @author David Gammon
 */
@RunWith(MockitoJUnitRunner.class)
public final class GitCommandsTest {
    /** Class under test. */
    private GitCommands gitCommands;
    
    /** Mock. */
    private @Mock CommandRunner commandRunner;
    
    /**
     * Setup method.
     * @throws CommandFailedException   Not thrown. Mock setup.
     */
    @Before
    public void setUp() throws CommandFailedException {
        gitCommands = new GitCommands(GenerateMojo.TAG_REGEX, GenerateMojo.LOG_REGEX, commandRunner);
        
        Mockito.when(commandRunner.runCommand("git tag --format=%(taggerdate:iso)%09%(refname:strip=2)"))
               .thenReturn("2019-06-04 06:00:48 +0100\tRELEASE-1.0.0\n2019-06-06 09:00:00 +0100\tRELEASE-1.1.0");
        Mockito.when(commandRunner.runCommand("git log --format=%B RELEASE-1.0.0")).thenReturn("*feature: some feature\n*bug: a bug\n");
        Mockito.when(commandRunner.runCommand("git log --format=%B RELEASE-1.0.0..RELEASE-1.1.0")).thenReturn("");
    }
    
    @Test
    public void getReleases_returnsReleases() throws CommandFailedException {
        final Release[] releases = gitCommands.getReleases();
        
        assertNotNull(releases);
        assertEquals(2, releases.length);
        
        assertEquals("RELEASE-1.0.0", releases[1].getName());
        assertEquals("1.0.0", releases[1].getVersion());
        
        final ReleaseItem[] items = releases[1].getItems();
        assertNotNull(items);
        assertEquals(2, items.length);
        assertEquals("Feature", items[0].getType());
        assertEquals("some feature", items[0].getDescription());
        assertEquals("Bug", items[1].getType());
        assertEquals("a bug", items[1].getDescription());
        
        assertEquals("RELEASE-1.1.0", releases[0].getName());
        assertEquals("1.1.0", releases[0].getVersion());
        
        assertNotNull(releases[0].getItems());
        assertEquals(0, releases[0].getItems().length);
    }
    
    @Test
    public void getReleases_nonRelease_doesNotReturn() throws CommandFailedException {
        Mockito.when(commandRunner.runCommand("git tag")).thenReturn("RELEASE-1.0.0\nRELEASE-1.1.0\nNon-Release");
        
        final Release[] releases = gitCommands.getReleases();
        
        assertNotNull(releases);
        assertEquals(2, releases.length);
        
        assertEquals("RELEASE-1.1.0", releases[0].getName());
        assertEquals("RELEASE-1.0.0", releases[1].getName());
    }
}
