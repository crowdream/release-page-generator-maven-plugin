/*******************************************************************************
 * Copyright (C) 2019 David Gammon
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
package io.cardiff.maven.releasegenerator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import io.cardiff.maven.releasegenerator.git.CommandFailedException;
import io.cardiff.maven.releasegenerator.git.GitCommands;
import io.cardiff.maven.releasegenerator.model.Release;
import io.cardiff.maven.releasegenerator.model.ReleaseItem;

/**
 * Unit test for {@link GenerateMojo}.
 * @author David Gammon
 */
@RunWith(MockitoJUnitRunner.class)
public final class GenerateMojoTest {
    /** Test data. */
    private static final String RELEASE_1_NAME = "RELEASE-1.0.0";
    /** Test data. */
    private static final String RELEASE_1_VERSION = "1.0.0";
    /** Test data. */
    private static final String RELEASE_2_NAME = "RELEASE-1.1.0";
    /** Test data. */
    private static final String RELEASE_2_VERSION = "1.1.0";
    /** Test data. */
    private static final String RELEASE_1_ITEM_1_TYPE = "Bug";
    /** Test data. */
    private static final String RELEASE_1_ITEM_1_DESC = "Fixed issue with css.";
    /** Test data. */
    private static final String RELEASE_1_ITEM_2_TYPE = "Feature";
    /** Test data. */
    private static final String RELEASE_1_ITEM_2_DESC = "User management";
    /** Test data. */
    private static final String RELEASE_2_ITEM_1_TYPE = "Feature";
    /** Test data. */
    private static final String RELEASE_2_ITEM_1_DESC = "Password reset";
    /** Test data. */
    private static final String DATE_1 = "08/08/2018";
    /** Test data. */
    private static final String DATE_2 = "10/09/2018";
    
    /** Class under test. */
    private GenerateMojo mojo;
    
    /** Writer used to get the generate source. */
    private StringWriter writer;
    
    /** Mock. */
    private @Mock GitCommands commands;
    
    /** Test data. */
    private Release release1;
    /** Test data. */
    private Release release2;
    
    /**
     * Setup this test.
     * @throws CommandFailedException   Not thrown. Mock setup. 
     * @throws ParseException           Thrown on data setup errors.
     */
    @Before
    public void setUp() throws CommandFailedException, ParseException {
        mojo = new GenerateMojo();
        
        writer = new StringWriter();
        
        mojo.setLogRegex(GenerateMojo.LOG_REGEX);
        mojo.setTagRegex(GenerateMojo.TAG_REGEX);
        mojo.setTemplate(GenerateMojo.TEMPLATE);
        mojo.setProperties(Arrays.asList("stylesheet=test.css"));
        
        mojo.loadWriter(writer);
        mojo.loadCommands(commands);
        
        final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        
        release1 = new Release(RELEASE_1_NAME, RELEASE_1_VERSION, format.parse(DATE_1));
        release1.addItem(new ReleaseItem(RELEASE_1_ITEM_1_TYPE, RELEASE_1_ITEM_1_DESC));
        release1.addItem(new ReleaseItem(RELEASE_1_ITEM_2_TYPE, RELEASE_1_ITEM_2_DESC));
        
        release2 = new Release(RELEASE_2_NAME, RELEASE_2_VERSION, format.parse(DATE_2));
        release2.addItem(new ReleaseItem(RELEASE_2_ITEM_1_TYPE, RELEASE_2_ITEM_1_DESC));
        
        Mockito.when(commands.getReleases()).thenReturn(new Release[] {release1, release2});
    }
    
    @Test
    public void execute_generatesHTML() throws MojoExecutionException, MojoFailureException {
        mojo.execute();
        
        final String result = writer.toString();
        
        assertTrue(result.contains("<link rel=\"stylesheet\" type=\"text/css\" href=\"test.css\"/>"));
        assertTrue(result.contains("<h1>1.0.0 (8 Aug, 2018)</h1>"));
        assertTrue(result.contains("<div class=\"item Bug\">Fixed issue with css.</div>"));
        assertTrue(result.contains("<div class=\"item Feature\">User management</div>"));
        
        assertTrue(result.contains("<h1>1.1.0 (10 Sep, 2018)</h1>"));
        assertTrue(result.contains("<div class=\"item Feature\">Password reset</div>"));
    }
    
    @Test
    public void execute_noStylesheet_doesNotIncludeHeader() throws MojoExecutionException, MojoFailureException {
        mojo.setProperties(Collections.emptyList());
        
        mojo.execute();
        
        final String result = writer.toString();
        
        assertFalse(result.contains("<link rel=\"stylesheet\""));
    }
}
